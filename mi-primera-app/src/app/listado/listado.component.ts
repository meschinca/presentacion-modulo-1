import { Component, OnInit, HostBinding } from '@angular/core';
import { Alumno } from '../model/alumno.model';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {
  @HostBinding('attr.class') cssClass = 'text-primary';
  alumnos: Alumno[];
  constructor() {
    this.alumnos = [];
  }

  ngOnInit(): void {
  }

  agregar(nombre: string, apellido: string): boolean {
    this.alumnos.push(new Alumno(nombre, apellido));
    console.log(this.alumnos);
    return false;
  }
}
