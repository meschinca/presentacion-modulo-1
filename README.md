# Proyecto Semana 1 #

Creación de SPA para implementar los contenidos de Angular vistos durante la primera semana del curso.

### Inicialización ###

* Descarga el repositorio
* Dentro de la carpeta "mi-primera-app" ejecuta las siguientes líneas de comando:

`npm i`

`ng serve`

* Ingresa en una ventana o nueva pestaña de tu navegador la URL: http://localhost:4200/


### Gracias por visitar mi repositorio ###